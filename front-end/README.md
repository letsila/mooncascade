# Sports Day Time Tracking Frontend

This is my technical test for the Mooncascade front-end hiring process.
This repository contains the tasks 2 and 3 of the take home test.

To run the app, please do the following action:

  1. Run `yarn` to install the dependencies
  2. Run `npm start` to run the server
  3. Open [http://localhost:3000](http://localhost:3000) to view it in the browser.
  4. Run the backend server, I have included the default server with no modification.
  5. Run the emulator of the backend server to watch the athelte list populated in real time with the fake data.
  6. Run `yarn test` to validate the task 3.


## `Task 3 validation`
To validate the task 3, I have created some integration tests in `/components/table.test.js`. This file contains some dummy captures of two fake athletes. At the start of the corridor

* athlete1 arrived first
* athlete2 arrived last

so the display is 

* athlete2
* athlete1

then at the finish line we check that the display is reversed because `athlete2` arrived first

* athlete1
* athlete2
