function pad(n) {
  return n < 10 ? '0' + n : n;
}

export function time(timestamp) {
  const date = new Date(timestamp);
  return `${pad(date.getHours())}:${pad(date.getMinutes())}:${pad(date.getSeconds())}.${date.getMilliseconds()}`;
}

export function milliseconds(captured) {
  return (new Date(captured)).getTime();
}