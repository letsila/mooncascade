import io from 'socket.io-client';

import * as TYPES from '../types';

const setupSocket = (dispatch) => {
  const socket = io('ws://localhost:5000');

  socket.on('connect', () => {
    console.log('connected');
  })

  socket.on('captures', (data) => {
    if (data[0]) {
      dispatch({
        type: TYPES.ADD_CAPTURE,
        ...data[0]
      })
    }
  })
}

export default setupSocket;