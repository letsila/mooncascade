import React from 'react';
import { Provider } from 'react-redux';
import { render } from 'react-testing-library'
import 'jest-dom/extend-expect'

import Table from './table';
import * as types from '../types';

import configureStore from '../configureStore';

// Populate readers
const readers = [
  {
    "id": 1,
    "position": 0,
    "name": "reader1",
    "event_id": 0
  },
  {
    "id": 2,
    "position": 1000,
    "name": "reader1",
    "event_id": 0
  }
];
const captures = [
  {
    "id": 0,
    "athlete": { "name": "athlete1", "number": 1, "id": 1 },
    "athlete_id": 1,
    "reader_id": 1,
    "timestamp": "2019-04-12T09:20:00.000Z",
    "captured": "2019-04-12T09:20:00.000Z"
  },
  {
    "id": 1,
    "athlete": { "name": "athlete2", "number": 2, "id": 2 },
    "athlete_id": 2,
    "reader_id": 1,
    "timestamp": "2019-04-12T09:20:10.100Z",
    "captured": "2019-04-12T09:20:10.100Z"
  },
  {
    "id": 2,
    "athlete": { "name": "athlete2", "number": 2, "id": 2 },
    "athlete_id": 2,
    "reader_id": 2,
    "timestamp": "2019-04-12T09:20:00.200Z",
    "captured": "2019-04-12T09:20:00.200Z"
  },
  {
    "id": 3,
    "athlete": { "name": "athlete1", "number": 1, "id": 1 },
    "athlete_id": 1,
    "reader_id": 2,
    "timestamp": "2019-04-12T09:20:00.300Z",
    "captured": "2019-04-12T09:20:00.300Z"
  }
];

const store = configureStore();

const [fist, second, third, fourth] = captures;

const renderComponent = () => render(
  <Provider store={store}>
    <Table />
  </Provider>
);
describe('testing rows ordering', () => {

  beforeEach(() => {
    store.dispatch({
      type: types.READERS_RECEIVED,
      readers: readers
    })
  })

  it('renders correct rows order in the corridor', () => {
    [fist, second].forEach((capture) => {
      store.dispatch({
        type: types.ADD_CAPTURE,
        athlete: capture.athlete,
        captured: capture.captured,
        reader_id: capture.reader_id,
        id: capture.id
      })
    })

    const { container } = renderComponent();

    const fistLi = container.querySelector('.athlete');
    expect(fistLi).toHaveAttribute('id', 'athlete2')

    const lastLi = container.querySelector('.athlete:last-child');
    expect(lastLi).toHaveAttribute('id', 'athlete1');
  })


  it('renders correct rows order at the finishline', () => {
    // At the start of the corridor
    [fist, second, third, fourth].forEach((capture) => {
      store.dispatch({
        type: types.ADD_CAPTURE,
        athlete: capture.athlete,
        captured: capture.captured,
        reader_id: capture.reader_id,
        id: capture.id
      })
    })

    const { container } = renderComponent();
    const lastLi = container.querySelector('.athlete:last-child');
    expect(lastLi).toHaveAttribute('id', 'athlete1');
  })
})