import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import { milliseconds } from '../utils';
import Row from './row';

class Table extends Component {

  getCapturesAtPosition(position) {
    if (this.props.readers.length && this.props.captures) {
      return Object.keys(this.props.captures).filter((id) => {
        return this.props.readers
          .filter(reader => reader.id === this.props.captures[id].reader_id)[0].position === position;
      });
    } else {
      return [];
    }
  }

  render() {
    const captures = this.props.captures;
    const capturesNotFinished = this.getCapturesAtPosition(0);
    const capturesFinished = this.getCapturesAtPosition(1000);

    return <ul className="table">
      <li className="colTitles">
        <span className="athleteNumber">Number</span>
        <span className="athleteName">Name</span>
        <span className="athleteTime">Time</span>
      </li>

      {capturesNotFinished.reverse().map(id => {
        return <Row key={id} data={captures[id]} finished={false} />
      })}

      {capturesFinished.map(id => captures[id])
        .sort((a, b) => milliseconds(b.captured) - milliseconds(a.captured))
        .map(capture => <Row key={capture.id} data={capture} finished={true} />)
      }
    </ul>
  }
}

const mapStateToProps = (state) => ({
  captures: state.captures,
  readers: state.readers
});

Table.propTypes = {
  captures: PropTypes.object.isRequired,
  readers: PropTypes.array.isRequired
}

export default connect(mapStateToProps)(Table);