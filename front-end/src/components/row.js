import React from 'react';
import PropTypes from 'prop-types';

import { time } from '../utils';

const Row = ({ data, finished }) => {
  return <li className="athlete" id={data.athlete.name}>
    <span className="athleteNumber">{data.athlete.number}</span>
    <span className="athleteName">{data.athlete.name}</span>
    <span className="athleteTime">
      {finished && time(data.captured)}
      {!finished && 'N/A'}
    </span>
  </li>
}

Row.propTypes = {
  data: PropTypes.object.isRequired,
  finished: PropTypes.bool.isRequired
}

export default Row;