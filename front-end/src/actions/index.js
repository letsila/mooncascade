import { call, put } from 'redux-saga/effects';

import * as types from '../types';

const api = (url) => fetch(url).then(response => response.json())

export const fetchReaders = () => ({
  type: types.FETCH_READERS
});

export function* fetchReader() {
  try {
    const readers = yield call(api, 'http://localhost:5000/readers')

    yield put({ type: types.READERS_RECEIVED, readers })
  } catch (e) {
    console.log(e)
  }
}