import { takeLatest } from 'redux-saga/effects';

import * as TYPES from '../types';
import { fetchReader } from '../actions';

function* mySaga() {
  yield takeLatest(TYPES.FETCH_READERS, fetchReader)
}

export default mySaga;