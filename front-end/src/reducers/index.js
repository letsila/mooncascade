import { combineReducers } from 'redux';

import * as types from '../types';

const readers = (state = [], action) => {
  switch (action.type) {
    case types.READERS_RECEIVED:
      return action.readers;
    default:
      return state;
  }
}

const captures = (state = {}, action) => {
  switch (action.type) {
    case types.ADD_CAPTURE:
      return {
        [action.athlete.id]: {
          athlete: action.athlete,
          captured: action.captured,
          reader_id: action.reader_id,
          id: action.id
        },
        ...state
      }
    default:
      return state;
  }
}

const reducers = combineReducers({
  captures,
  readers
})


export default reducers;