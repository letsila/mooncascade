import React, { Component } from 'react';
import { connect } from 'react-redux';

import { fetchReaders } from './actions';
import Table from './components/table'
import './App.scss';


class App extends Component {

  componentDidMount() {
    this.props.fetchReaders();
  }

  render() {
    return (
      <div className="App">
        <Table />
      </div>
    );
  }
}

export default connect(null, { fetchReaders })(App);
