import { createStore, compose, applyMiddleware } from 'redux';
import createSagaMiddleware from 'redux-saga';

import mySaga from './sagas';
import reducers from './reducers';

const configureStore = () => {
  const sagaMiddleware = createSagaMiddleware();

  const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
  const store = createStore(reducers, composeEnhancers(
    applyMiddleware(sagaMiddleware)
  ));


  sagaMiddleware.run(mySaga);

  return store;
}

export default configureStore;